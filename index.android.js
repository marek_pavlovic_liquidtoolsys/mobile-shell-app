/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  WebView,
  View
} from 'react-native';

export default class LTSshell extends Component {
  render() {
    return (
	<View style={styles.container}>
	    <WebView
//        	source={require('./lts.html')}   http://stackoverflow.com/questions/37102662/external-html-asset-not-bundled-by-react-native-in-production-build-for-use-by-w
		source={{uri: 'file:///android_asset/lts.html'}}
	        style={styles.webView} />
	</View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
	flex:1
  },
  webView: {
    backgroundColor: '#fff'
  }
});

AppRegistry.registerComponent('LTSshell', () => LTSshell);
